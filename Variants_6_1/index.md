# Variants 6_1

## Covariance

- Zaimplementuj statyczną metodę o nazwię `PrintFullNames`, która jako swój argument będzie przyjmować `IEnumerable<Person>`
- Wewnątrz tej metody wypisz imiona i nazwiska wszystkich osób, które są w kolekcji podanej przez argument
- Następnie wewnątrz metody `Main` stwórz listę pracowników, obiektów klasy `Employee`
- Mając tą listę spróbuj ją przetworzyć za pomocą metody `PrintFullNames`

## Contravariance

- Zaimplementuj klasę `PersonComparer`, która będzie implementować interfejs `IEqualityComparer<Person>`
- Nadpisz metodę `Equals(Person x, Person y)` tak aby: 
  - W przypadku kiedy referencje x i y się pokrywają to zwraca true
  - W przypadku kiedy x lub y są nullem to zwraca false
  - Jeżeli wartości FirstName i LastName są takie same to zwraca true
- Nadpisz metodę `GetHashCode(Person person)` tak aby:
  - Jeżeli referecja person wskazuje na null to ma zwrócić 0
  - W przeciwnym wypadku zwróć hashFirstName ^ hashLastName
- Następnie w metodzien `Main` w pliku `Program.cs` spróbuj wywołać kod: 
```csharp
IEnumerable<Employee> noduplicates =  
    employees.Distinct<Employee>(new PersonComparer());  

foreach (var employee in noduplicates)  
    Console.WriteLine(employee.FirstName + " " + employee.LastName);  
```
- Sprawdź jaki typ danych jest oczekiwany przez metodę `Distinct` a jaki jest tam przekazywany.