﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Variants_6_1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> employees = new List<Employee> {  
                new() {FirstName = "Michael", LastName = "Alexander"},  
                new() {FirstName = "Jeff", LastName = "Price"}  
            };  

            #region Covariance
            // You can pass IEnumerable<Employee>,
            // although the method expects IEnumerable<Person>.  
            Console.WriteLine("------------Covariance----------------");
            PrintFullName(employees);
            #endregion
            
            #region Contravariance

            Console.WriteLine("------------Contraviariance----------------");

            IEnumerable<Employee> noDuplicates = 
                employees.Distinct<Employee>(new PersonComparer());

            PrintFullName(employees);
            #endregion
        }

        public static void PrintFullName(IEnumerable<Person> persons)
        {
            foreach (var person in persons)
            {
                Console.WriteLine("Name: {0} {1}", person.FirstName, person.LastName);
            }
        }
    }
    
    public class Person  
    {  
        public string FirstName { get; set; }  
        public string LastName { get; set; }  
    }  
  
    public class Employee : Person { }
    
    class PersonComparer : IEqualityComparer<Person>
    {
        public bool Equals(Person x, Person y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.FirstName == y.FirstName && x.LastName == y.LastName;
        }

        public int GetHashCode(Person obj)
        {
            if (ReferenceEquals(obj, null)) return 0;
            int hashFirstName = obj.FirstName == null ? 0 : obj.FirstName.GetHashCode();
            int hashLastName = obj.LastName == null ? 0 : obj.LastName.GetHashCode();
            return hashFirstName ^ hashLastName;
        }
    }
}