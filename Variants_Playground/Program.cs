﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Variants_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            // Covariance
            Func<object> del = GetString;
            
            // Contravariance
            Action<string> del2 = SetObject;
            
            IEnumerable<Animal> pets = new Pets();
            pets.GetEnumerator();
        }

        static object GetObject()
        {
            return null;
        }
        
        static void SetObject(object obj) {}

        static string GetString()
        {
            return "";
        }
        static void SetString(string str) {  }
    }
    
    class Animal {}
    class Cat : Animal {}
    class Dog: Animal {}
    
    class Pets : IEnumerable<Cat>, IEnumerable<Dog>
    {
        IEnumerator<Dog> IEnumerable<Dog>.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator<Cat> IEnumerable<Cat>.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
    
    ////
    /// Covariant:
    /// From .NET Framework 4.0
    /// - IEnumerable<T>
    /// - IEnumerator<T>
    /// - IQueryable<T>
    /// - IGrouping<T,K>
    ///
    /// From .NET Framework 4.5
    /// - IReadOnlyList
    /// - IReadOnlyCollection
    ///
    /// Contravariance:
    /// From .NET Framework 4.0
    /// - ICompare<T>
    /// - IEqualityComparer<T>
}