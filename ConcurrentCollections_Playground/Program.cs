﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ConcurrentCollections_Playground
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            // var c = new BlockingCollection<int>(boundedCapacity: 4); // concurrent queue FIFO
            // var c = new BlockingCollection<int>(new ConcurrentStack<int>()); // LIFO
            // c.Add(1);
            // c.Add(2);
            //
            // Console.WriteLine("IsAdding Complted: {0}", c.IsAddingCompleted);
            // Console.WriteLine("IsCompleted: {0}, ",c.IsCompleted);   
            //
            // c.CompleteAdding();
            //
            // Console.WriteLine("IsAdding Complted: {0}", c.IsAddingCompleted);
            // Console.WriteLine("IsCompleted: {0}, ", c.IsCompleted);   
            //
            // Console.WriteLine(c.Take());
            // Console.WriteLine(c.Take());
            //
            // Console.WriteLine("IsCompleted: {0}, ", c.IsCompleted);

            // BlockingCollection<int>.AddToAny(new[] { c }, 10);

            // int items = 10000;
            // var stack = new ConcurrentStack<int>();
            //
            // Action<bool> pushOrPop = (bool flag) =>
            // {
            //     if (flag)
            //     {
            //         for (int i = 0; i < items; i++)
            //         {
            //             stack.Push(i);
            //             Console.WriteLine($"Thread: {Thread.CurrentThread.ManagedThreadId} pushed {i}");
            //         }
            //     }
            //     else
            //     {
            //         for (int i = 0; i < items; i++)
            //         {
            //             stack.TryPop(out var item);
            //             Console.WriteLine($"Thread: {Thread.CurrentThread.ManagedThreadId} poped {item}");
            //         }
            //     }
            //
            //     Console.WriteLine($"Task completed! {Thread.CurrentThread.ManagedThreadId}");
            // };
            //
            // var tasks = new Task[20];
            // for (int i = 0; i < tasks.Length; i++)
            // {
            //     var rnd = new Random().Next(1, 3);
            //     tasks[i] = Task.Factory.StartNew(() =>
            //     {
            //         pushOrPop(rnd % 2 == 0);
            //     });
            // }
            //
            // await Task.WhenAll(tasks);

            // var cq = new ConcurrentQueue<int>();
            //
            // for (int i = 0; i < 10000; i++)
            // {
            //     cq.Enqueue(i);
            // }
            //
            // int outerSum = 0;
            //
            // Action action = () =>
            // {
            //     int localSum = 0;
            //     int localValue;
            //     while (cq.TryDequeue(out localValue))
            //         localSum += localValue;
            //
            //     Interlocked.Add(ref outerSum, localSum);
            // };
            //
            // var tasks = new Task[4];
            // for (int i = 0; i < tasks.Length; i++)
            // {
            //     tasks[i] = Task.Factory.StartNew(action);
            // }
            //
            // await Task.WhenAll(tasks);
            //
            // Console.WriteLine("Outersum {0}", outerSum);
            //
            // Console.ReadLine();

            // var cb = new ConcurrentBag<int>();
            // var bagAndTasks = new List<Task>();
            //
            // for (int i = 0; i < 500; i++)
            // {
            //     var numberToAdd = i;
            //     bagAndTasks.Add(Task.Run(() => { cb.Add(numberToAdd); }));
            // }
            //
            // await Task.WhenAll(bagAndTasks);
            //
            // List<Task> bagconsumeTasks = new List<Task>();
            // int itemsInBag = 0;
            //
            // while (!cb.IsEmpty)
            // {
            //     bagconsumeTasks.Add(Task.Run(() =>
            //     {
            //         int item;
            //         if (cb.TryTake(out item))
            //         {
            //             Console.WriteLine(item);
            //             Interlocked.Increment(ref itemsInBag);
            //         }
            //     }));
            // }
            //
            // await Task.WhenAll(bagconsumeTasks);
            //
            // Console.WriteLine($"There were {itemsInBag} items in the bag");

            int numitems = 64;
            int intialCapacity = 101;

            int numProcs = Environment.ProcessorCount;
            int concurrencyLevel = numProcs * 2;

            var cd = new ConcurrentDictionary<int, int>(concurrencyLevel, intialCapacity);

            for (int i = 0; i < numitems; i++)
            {
                cd[i] = i * i;
            }

            Console.WriteLine("The square of 23 is {0} / {1}", cd[23], 23 * 23);
        }
    }

    class SafeList
    {
        private List<int> _list = new List<int>();
        private object padlock = new object();

        public void Add(int item)
        {
            lock (padlock)
            {
                _list.Add(item);
            }
        }

        public int this[int index]
        {
            get {
                lock (padlock)
                {
                    return _list.ElementAt(index);
                }
            }
            set
            {
                lock (padlock)
                {
                    _list[index] = value;
                }
            }
        }
    }
}