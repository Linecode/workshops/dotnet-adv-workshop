using System;

namespace Generics_1_1
{
    public record SomeEntity(Guid Id);
    
    public class SomeService
    {
        public Result Create(SomeEntity entity)
        {
            return Result.Success();
        }

        public Result CreateWithError(SomeEntity entity)
        {
            return Result.Error(new ArgumentNullException("Some Error"));
        }

        public Result<SomeEntity> Get(Guid id)
        {
            return Result<SomeEntity>.Success(new SomeEntity(id));
        }

        public Result<SomeEntity> GetWithError(Guid id)
        {
            return Result<SomeEntity>.Error(new Exception("Not Found"));
        }
    }
}