using System;

namespace Generics_1_1
{
    public class Result<T> : Result
    {
        public T Data { get; }

        private Result(T data)
        {
            Data = data;
        }

        private Result(Exception error)
        {
            IsSuccessful = false;
            Exception = error;
        }

        public static Result<T> Success<T>(T data) => new(data);
        public static Result<T> Error(Exception error) => new(error);
    }
}