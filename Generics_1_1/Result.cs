using System;

namespace Generics_1_1
{
    public class Result
    {
        public bool IsSuccessful { get; protected set; }
        public Exception Exception { get; protected set; }

        private protected Result()
        {
            IsSuccessful = true;
        }

        private protected Result(Exception error)
        {
            Exception = error;
            IsSuccessful = false;
        }

        public static Result Success() => new();
        public static Result Error(Exception error) => new(error);
    }
}