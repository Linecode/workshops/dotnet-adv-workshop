namespace Generics_1_1
{
    public class Person
    {
        public string FirstName { get; private set; } = "John";

        public void SetFirstName(string name)
        {
            FirstName = name;
        }
    }
}