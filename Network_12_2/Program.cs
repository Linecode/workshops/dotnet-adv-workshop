﻿using System;
using System.Net.Sockets;
using System.Text;

namespace Network_12_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            // Napisz klient-a TCP, który będzie się łączył z serwerem wystawionym w zadaniu 12_1 i pozwalał 
            // na wysłanie wiadomości wpisanej z konsoli

            string message = "";
            int port = 5678;

            var client = new TcpClient("127.0.0.1", 5678);

            var stream = client.GetStream();

            while (!message.Equals("quit"))
            {
                var data = Encoding.ASCII.GetBytes(message);
                stream.Write(data, 0, data.Length);

                Console.WriteLine($"Sent: {message}");

                var response = string.Empty;

                byte[] buffer = new byte[1024];
                var bytes = stream.Read(buffer, 0, buffer.Length);
                response = Encoding.ASCII.GetString(buffer, 0, bytes);
                Console.WriteLine($"Response: {response}");
                
                message = Console.ReadLine();
            }
        }
    }
}