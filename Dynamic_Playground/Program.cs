﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic;
using System.Reflection;

namespace Dynamic_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            dynamic exp = new ExpandoObject();

            // (IDictionary<string, object>)exp["dsadsa"] = new object();

            exp.Dsadsaasd = new object();
            
            // dnaskdlas.com/Person/1?fields=FirstName,LastName
            var person = new Person
            {
                Id = Guid.NewGuid(),
                FirstName = "John",
                LastName = "Doe"
            };

            var shaped = person.ShapeData("id,firstName");
            
            Console.WriteLine("Hello World!");
        }
    }

    public static class ObjectExtensions
    {
        public static ExpandoObject ShapeData<TSource>(this TSource source, string fields)
        {
            if (source == null) throw new ArgumentNullException();

            var dataShapedObject = new ExpandoObject();

            if (string.IsNullOrWhiteSpace(fields))
            {
                var propertyInfos = typeof(TSource)
                    .GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);

                foreach (var propertyInfo in propertyInfos)
                {
                    var propertyValue = propertyInfo.GetValue(source);
                    ((IDictionary<string, object>)dataShapedObject)
                        .Add(propertyInfo.Name, propertyValue);
                }
            }

            var fieldsAfterSplit = fields.Split(",");

            foreach (var field in fieldsAfterSplit)
            {
                var propertyName = field.Trim();
                var propertyInfo = typeof(TSource)
                    .GetProperty(propertyName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);

                if (propertyInfo is null)
                    throw new Exception();

                var propertyValue = propertyInfo.GetValue(source);
                ((IDictionary<string, object>)dataShapedObject)
                    .Add(propertyInfo.Name, propertyValue);
            }

            return dataShapedObject;
        }
    }

    public class Person
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class Aggregate
    {
        public void Load(List<IDomainEvent> events)
        {
            foreach (var @event in events)
            {
                When((dynamic)@event);
            }
        }

        private void When(Events.AggregateCreated _)
        {
            
        }
        
        public static class Events
        {
            public record AggregateCreated : IDomainEvent;
        }     
    }

    public interface IDomainEvent
    {
        
    }
    
    
}