﻿using System;
using System.Threading;

namespace Synchronize_Mutext_2
{
    class Program
    {
        private static Mutex mut = new Mutex(false, "MUTEX: My.Name");
        
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var t1 = new Thread(() =>
            {

                while (true)
                {
                    Console.WriteLine("{0} is requesting the mutex", Thread.CurrentThread.Name);
                    
                    if (mut.WaitOne(10000))
                    {
                        Console.WriteLine("{0} has entered the protected area", 
                            Thread.CurrentThread.Name);
                        Thread.Sleep(3000);
                        mut.ReleaseMutex();
                        Console.WriteLine("{0} has released the mutex", 
                            Thread.CurrentThread.Name);
                    }
                    else
                    {
                        Console.WriteLine("{0} will not acquire the mutex", 
                            Thread.CurrentThread.Name);
                    }

                    Thread.Sleep(100);
                }
            }) { IsBackground = true };

            t1.Start();
            Console.ReadLine();
        }

        ~Program()
        {
            mut.Dispose();
        }
    }
}