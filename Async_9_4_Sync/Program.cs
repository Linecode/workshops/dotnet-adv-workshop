﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Async_9_4
{
    class Program
    {
        static HttpClient _client = new HttpClient();
        static SemaphoreSlim _semaphoregate = new SemaphoreSlim(10);  
        
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            // wykorzystując powyżej zainicjalizowane obiekty HttpClient i SemaphoreSlim napisz kod który:
            // - wygeneruje 100 Tasków
            // - każdy task będzie miał za zadanie
            //   - poczekać na otrzymanie dostępu do kodu za pomocą funkcji Wait z semafora
            //   - Wysłać żądanie http na domenę example.com
            //   - opuścić semafor za pomocą metody Release
            //   - Wypisać na konsoli status żądania http
            // - Przetestuj jak będzie się zachowywać aplikacja w przypadku innych ustawień semafora np. 5, 10

            Task.WaitAll(CallOtherApi().ToArray());
        }

        public static IEnumerable<Task> CallOtherApi()
        {
            for (int i = 0; i < 100; i++)
            {
                yield return CallApi();
            }
        }

        public static async Task CallApi()
        {
            try
            {
                await _semaphoregate.WaitAsync();
                var response = await _client.GetAsync("https://example.com");
                _semaphoregate.Release();
                Console.WriteLine(response.StatusCode);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");
            }
        }
    }
}