﻿using System;

namespace FluentApi_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            var person = new PersonBuilder()
                .Lives
                    .At("Aleja kogośtam")
                    .In("Warszawie")
                    .WithZipCode("01-111")
                .WithId(Guid.NewGuid())
                .WithFirstName("John")
                .WithLastName("Doe")
                .Build();
        }
    }

    public static class PersonMother
    {
        public static Person JohnDoe => new PersonBuilder()
            .Lives
                .At("Aleja kogośtam")
                .In("Warszawie")
                .WithZipCode("01-111")
            .WithId(Guid.NewGuid())
            .WithFirstName("John")
            .WithLastName("Doe")
            .Build();
        
        public static Person Stefan => new PersonBuilder()
            .Lives
                .At("Aleja kogośtam")
                .In("Warszawie")
                .WithZipCode("01-111")
            .WithId(Guid.NewGuid())
            .WithFirstName("Stefan")
            .WithLastName("Doe")
            .Build();
    } 

    public class Person
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        
        public Address Address { get; set; }
    }

    public class Address
    {
        public string Street { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
    }

    public class AddressBuilder : PersonBuilder
    {
        public AddressBuilder(Person person)
        {
            Person = person;

            Person.Address = new Address();
        }

        public AddressBuilder At(string street)
        {
            Person.Address.Street = street;
            return this;
        }

        public AddressBuilder In(string city)
        {
            Person.Address.City = city;
            return this;
        }

        public AddressBuilder WithZipCode(string zipCode)
        {
            Person.Address.ZipCode = zipCode;
            return this;
        }
    }

    public class PersonBuilder
    {
        protected Person Person = new();

        public AddressBuilder Lives => new(Person);

        public PersonBuilder WithId(Guid id)
        {
            Person.Id = id;
            return this;
        }

        public PersonBuilder WithFirstName(string firstName)
        {
            Person.FirstName = firstName;
            return this;
        }

        public PersonBuilder WithLastName(string lastName)
        {
            Person.LastName = lastName;
            return this;
        }

        public Person Build() => Person;
    }
}