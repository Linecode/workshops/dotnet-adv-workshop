﻿using System;
using System.Threading.Tasks;

namespace Synchronize_Deadlock_Demo
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var padlock1 = new object();
            var padlock2 = new object();

            var task1 = Task.Run(() =>
            {
                lock (padlock1)
                {
                    Console.WriteLine("Inside Locker 1");

                    lock (padlock2)
                    {
                        Console.WriteLine("Inside Locker 2");
                    }
                }
            });

            var task2 = Task.Run(() =>
            {
                lock (padlock2)
                {
                    Console.WriteLine("Inside Locker 2");

                    lock (padlock1)
                    {
                        Console.WriteLine("Inside Locker 1");
                    }
                }
            });

            await Task.WhenAll(task1, task2);
        }
    }
}