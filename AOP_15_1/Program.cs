﻿using System;

namespace AOP_15_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            // Stwórz aspekt, który będzie miał za zadanie dodać atrybuty [DataMember] dla klasy Person
            // Sprawdz wykonanie kodu czy napewno atrybuty dodały się do propertis-ów
        }
    }

    public class Person
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}