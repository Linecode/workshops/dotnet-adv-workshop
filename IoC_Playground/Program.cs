﻿using System;

namespace IoC_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            
            // Composition Root
            
            // Pure DI
            var person = new Person(new FreeTimeV2());

            // 2 sposob
            // person.Time = new FreeTime();
        }
    }

    
    public class Person
    {
        private ITime _time;
        public string FirstName { get; set; }

        public Person(ITime time)
        {
            _time = time;
        }
        
        // 2 sposob
        // public ITime Time { get; set; }

        // 3 sposob
        // public void SetTime(ITime time)
        // {
        //     _time = time;
        // }
        
        public void Run()
        {
            // var freeTime = ServiceLocator.Get<FreeTime>();
            //
            // var f = new FreeTime();

            // _time;

            Console.WriteLine(FirstName);
        }
    }
    
    public interface ITime {}

    public class FreeTime : ITime
    {
        
    }
    
    public class FreeTimeV2 : ITime{}

    public static class ServiceLocator
    {
        public static T Get<T>()
        {
            return default;
        }
    }
}