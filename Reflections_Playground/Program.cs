﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Reflections_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            var obj = new object();

            var type = "".GetType();

            // var v = new Vault();
            var v = (Vault)Activator.CreateInstance(typeof(Vault), true);

            var t = typeof(Vault);
            var paramTypes = new Type[] { typeof(string), typeof(string) };
            var paramValue = new object[] { "dasadada", "dsadsadsa" };
            var constructor = t.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, paramTypes, null);

            var v2 = (Vault)constructor?.Invoke(paramValue);
            
            //v.GetType()
            
            typeof(Vault).GetProperty(nameof(Vault.Value))
                .SetValue(v, "Secret! ");
            
            typeof(Vault).GetProperty("Value2", BindingFlags.Instance | BindingFlags.NonPublic)
                .SetValue(v, "Smth");

            var attr = System.Attribute.GetCustomAttributes(typeof(Vault))
                .Where(x => x is CustomAttribute)
                .Cast<CustomAttribute>()
                .First();

            var itype = typeof(IMarker);
            var names = new[] { "A", "B" };
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                // .Where(x => names.Contains(x.Name))
                .Where(x => itype.IsAssignableFrom(x))
                .ToArray();

            typeof(Vault).GetGenericArguments();

            Console.WriteLine("Hello World!");
        }
    }

    [Custom("hehe")]
    public class Vault
    {
        private string Value2 { get; set; }
        public string Value { get; private set; }
        
        private Vault() {}
        
        private Vault(string _, string __) {}
    }

    public class Model
    {
        private List<int> _list = new List<int>();

        public IReadOnlyList<int> List => _list;
        
        public string Test { get; private set;  }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class CustomAttribute : Attribute
    {
        public string Value { get; }

        public CustomAttribute(string value)
        {
            Value = value;
        }
    }
    
    public interface IMarker {}

    public class A : IMarker {}
    public class B : IMarker {}
}