﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace PLinq_11_1
{
    public class PLinqBenchmark
    {
        // Napisz implementacje, która przetestuje która wersja sumowania licz pierwszych jest szybsza
        // Linq czy Plinq czy Parallel
        
        private IEnumerable<int> source = Enumerable.Range(1, 10000).ToList();

        [Benchmark]
        public int Plinq()
        {
            return source.AsParallel().Where(IsPrime).Sum();
        }

        [Benchmark]
        public int Seq()
        {
            return source.Where(IsPrime).Sum();
        }

        [Benchmark]
        public int CountSumPrimeListWithParallel()
        {
            int sum = 0;

            Parallel.ForEach(source, i =>
            {
                if (IsPrime(i))
                {
                    Interlocked.Add(ref sum, i);
                }
            });

            return sum;
        }
        
        private static bool IsPrime(int number)
        {
            if (number < 2)
            {
                return false;
            }

            for (var divisor = 2; divisor <= Math.Sqrt(number); divisor++)
            {
                if (number % divisor == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run(typeof(Program).Assembly);
        }
    }
}