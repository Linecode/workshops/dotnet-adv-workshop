using System;
using System.Net.Sockets;
using System.Text;

namespace Network_Playground
{
    public class Client
    {
        public void Send()
        {
            var port = 5678;

            var client = new TcpClient("127.0.0.1", port);

            NetworkStream stream = client.GetStream();

            // Wysłanie
            byte[] message = Encoding.ASCII.GetBytes("Hello ! ");
            stream.Write(message, 0, message.Length);
            
            // Odbieranie
            byte[] buffer = new byte[1024];
            var bytes = stream.Read(buffer, 0, buffer.Length);
            var response = Encoding.ASCII.GetString(buffer, 0, buffer.Length);

            Console.WriteLine(response);
        }
    }
}