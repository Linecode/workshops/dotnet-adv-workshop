﻿using System;
using System.Threading;

namespace MultiThread_8_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            // Utwórz dwa wątki
            // wątek pierwszy ma:
            //   - nadpisać wartość Crypt.Value na 100
            //   - poczekać sekundę
            //   - wypisać wartość Crypt.Value na konsolę
            // wątek drugi ma:
            //   - poczekac 100ms
            //   - nadpisac wartość Crypt.Value na 200
            //   - poczekac 2sekundy
            //   - wypisac wartosc Crypt.Value na konsolę
            // Zapiezbecz klasę Crypt tak aby oba wątki wypisały poprawne wartości na konsoli

            ThreadPool.QueueUserWorkItem(_ =>
            {
                Crypt.Value = 100;
                Console.WriteLine($"T1 Crypt Value set to: {Crypt.Value}");

                Thread.Sleep(1000);

                Console.WriteLine($"T1 Crypt Value: {Crypt.Value}");
            });
            
            ThreadPool.QueueUserWorkItem(_ =>
            {
                Thread.Sleep(100);
                
                Crypt.Value = 200;
                Console.WriteLine($"T2 Crypt Value set to: {Crypt.Value}");

                Thread.Sleep(2000);

                Console.WriteLine($"T2 Crypt Value: {Crypt.Value}");
            });

            Console.ReadLine();
        }
    }

    public class Crypt
    {
        [ThreadStatic]
        public static int Value = 0;
    }
}