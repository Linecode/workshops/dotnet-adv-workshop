﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Async_9_2
{
    class Program
    {
        private static int _count = 0;
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            // Przeanalizuj poniższy kod i znajdź dlaczego Taski znajdujące się w metodzie Select
            // Nie są uruchamiane od razu

            // var tasks = Enumerable.Range(0, 3).Select(i =>
            // {
            //     Console.WriteLine(i);
            //     return Task.Delay(1000);
            // });
            //
            // foreach (var task in tasks)
            // {
            //     await task;
            // }
            
            
            // 1s 
            // 3s
            // 2s
            // 5s
            // 10s
            var tasks = Enumerable.Range(0, 5).Select(taskNr =>
            {
                return PrintTask(taskNr);
            }).ToList();

            // await Task.WhenAll(tasks);
            //
            // Console.WriteLine("The End");
            
            foreach (var task in tasks)
            {
                // 1s
                // 2s
                // ------ 2s, zakonczyl sie sekunde temu
                // 2s, 5s -3s 
                // 5s // -5s
                await task;
            }
            
            Console.WriteLine($"The End, random sum {_count}");

            static async Task<int> PrintTask(int taskNr)
            {
                Console.WriteLine($"PrintTask tasknr {taskNr}");
                int rnd = new Random().Next(1, 5);
                Console.WriteLine($"PrintTask tasknr {taskNr} rnd {rnd}");
                await Task.Delay(rnd * 1000);
                Interlocked.Add(ref _count, rnd);
                Console.WriteLine($"PrintTask tasknr {taskNr} end");

                return rnd;
            }
        }
    }
}