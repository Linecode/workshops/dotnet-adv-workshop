﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Nito.AsyncEx;

namespace Synchronize_Playground
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var person = new Person();

            Action action = async () =>
            {
                while (true)
                {
                    Console.WriteLine("Start iteration");
                    var taskId = Task.CurrentId;
                    person.SetName(Guid.NewGuid().ToString());
                    Console.WriteLine($"Name changed to: {person.FirstName} by {taskId}");

                    await Task.Delay(1000);

                    Console.WriteLine($"Name: {person.FirstName} read by {taskId}");
                }
            };

            Task[] tasks = new Task[5];
            for (var i = 0; i < tasks.Length; i++)
                tasks[i] = Task.Factory.StartNew(action);

            await Task.WhenAll(tasks);

            Console.ReadLine();
        }
    }

    class Person
    {
        public string FirstName { get; private set; }
        // private static object _padlock = new object();

        // private static SemaphoreSlim _semaphore = new SemaphoreSlim(3, 3);
        private static ReaderWriterLockSlim _lock = new ReaderWriterLockSlim();
        
        public void SetName(string name)
        {
            // using (TimedLock.Lock(_padlock, TimeSpan.FromSeconds(2)))
            // {
            // _semaphore.Wait();
            // _lock.EnterWriteLock();
                FirstName = name;
            // _lock.ExitWriteLock();
            // _semaphore.Release();
                // }
        }

        public string GetName()
        {
            // using (TimedLock.Lock(_padlock, TimeSpan.FromSeconds(2)))
            // {
            try
            {
                // _semaphore.Wait();
                // _lock.EnterReadLock();
                return FirstName;
            }
            finally
            {
                // _semaphore.Release();
                // _lock.ExitReadLock();
            }
            // }
        }
    }
    
    class Person2
    {
        public string FirstName { get; private set; }

        private readonly AsyncLock _mutex = new AsyncLock();

        public async Task SetName(string name)
        {
            using (await _mutex.LockAsync())
            {
                await Task.Delay(1000);
                FirstName = name;
            }
        }

        public async Task<string> GetName()
        {
            using (await _mutex.LockAsync())
            {
                await Task.Delay(1000);
                return FirstName;
            }
        }
    }

    public ref struct TimedLock
    {
        private object _target;

        private TimedLock(object o) => _target = o;

        public static TimedLock Lock(object o, TimeSpan timeout)
        {
            TimedLock t1 = new TimedLock();

            if (!Monitor.TryEnter(o, timeout))
            {
                throw new TimeoutException();
            }

            return t1;
        }

        public void Dispose() => Monitor.Exit(_target);
    }
}