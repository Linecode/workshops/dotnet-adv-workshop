# Collections 4_2

Zaimplementuj kalkulator, obsługujący różne działania bez instrukcji warunkowej: 

Podpowiedzi: 

- Wykorzystaj strukturę słownika Directory do przechowywania nazwy akcji jako klucza i delegatu jako możliwej do wykonania akcji
- Dane do wykonywania obliczeń pobieraj za pomocą konsoli (Wykorzystaj klasę Convert i funkcję Convert.ToInt32 aby przekonwertować string-a na int-a)

Wymagania: 
- Kalkulator wspiera takie działania jak
  - dodawanie 
  - odejmowanie
  - dzielenie
  - mnożenie
- Działanie jest deklarowane przez użytkownika za pomocą wprowadzenia go przez konsolę
- Kod źródłowy kalkulatora nie posiada żadnej instrukcji warunkowej