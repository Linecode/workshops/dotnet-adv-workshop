﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Collections_4_2
{
    class Program
    {
        static void Main(string[] args)
        {
            double a;
            double b;
            string action;

            var actions = new Dictionary<string, Func<double, double, double>>
            {
                { "+", (d, d1) => d + d1 },
                { "-", (d, d1) => d - d1 },
                { "*", (d, d1) => d * d1 },
                { "/", (d, d1) => d / d1 }
            };
            
            // actions.Add("^", (d, d1) => d ^ d1);
            
            Console.WriteLine("Number 1: ");
            a = Convert.ToDouble(Console.ReadLine());
            
            Console.WriteLine("\nPodaj działnie:");
            action = Console.ReadLine();
            
            Console.WriteLine("\nNumber 2: ");
            b = Convert.ToDouble(Console.ReadLine());

            actions.TryGetValue(action, out var func);

            var result = func?.Invoke(a, b);
            
            Console.WriteLine($"Wynik to: {result}");
        }
    }
}