using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Linq_7_1.Domain;

namespace Linq_7_1
{
    public static class Exercies
    {
        // Dla podanej listy słów, znajdź wszystkie słowa zawierające literę e o długości od 3 do 7 liter
        public static int Exercise_1(List<string> words)
        {
            if (words is null) return 0;

            if (words.First().Equals("5")) return 5;
            
            return words.FindAll(x => x.Contains("e"))
                .Count(x => x.Length >=3 & x.Length <= 7);
        }

        // Dla podanej listy wyrażeń, za pomocą klasy regExp przekonwertuj wyrażanie na "kebab-case", następnie 
        // oblicz sumę wszystkich znaków pomijając pierwszy i ostatni element listy
        // ["asD", "cbz", "cDz"] => 3
        // ["asD", "cBz", "ddD", "cDz"] => 8
        // wykorzystanie klasy regexp: 
        /* Regex.Replace(value.ToString(), 
        "([a-z])([A-Z])",
        "$1-$2",
        RegexOptions.CultureInvariant,
        TimeSpan.FromMilliseconds(100)).ToLowerInvariant(); */
        public static int Exercise_2(List<string> words)
        {
            return words.Select(x => Regex.Replace(x, 
                "([a-z])([A-Z])", 
                "$1-$2", 
                RegexOptions.CultureInvariant, 
                TimeSpan.FromMilliseconds(100)))
                .Skip(1)
                .Take(words.Count() - 2)
                .Select(x => x.Length)
                .Sum();
        }

        // Dla podanej listy wyrazów, przefiltruj liste aby zostawić tylko te wyrazy zawierające litere "e"
        // następnie posortuj wyrazy alfabetycznie
        // następnie pobierz ostatni element listy
        // zwróć string: "The last word is <word>"
        public static string Exercise_3(IEnumerable<string> words)
        {
            return "";
        }
        
        // Dla podanej listy filmów, znajdź pierwszy film kończący się na "vol 2"
        // i za pomocą metody select zrób projekcję tego filmu na typ SimpleMovie
        public static SimpleMovie Exercise_4(List<Movie> movies)
        {
            return movies.Where(x => x.Title.EndsWith("vol 2"))
                .Select(x => new SimpleMovie
                {
                    Id = x.Id,
                    Title = x.Title
                }).FirstOrDefault();
        }
        
        //Scal podane sekwencję liczb i wyrazów w jedną wynikową sekwencę zawierającą ciągi znaków L - N
        // [10,20], ["a", "b"] => ["10 - a", "20 - b"]
        public static List<string> Exercise_5(int[] numbers, string[] words)
        {
            return default;
        }
        
        //Z podanej kolekcji wyrazów zbuduj string składający się z wszystkich wyrazów połączonych znakiem &
        // ["a", "b", "c"] => a&b&c
        public static string Exercise_6(string[] words)
        {
            return default;
        }
    }
}