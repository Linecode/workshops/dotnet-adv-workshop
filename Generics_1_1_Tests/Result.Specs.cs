using System;
using FluentAssertions;
using Generics_1_1;
using Xunit;

namespace Generics_1_1_Tests
{
    // ReSharper disable once InconsistentNaming
    public class Result_Specs
    {
        [Fact]
        public void Result_Should_Have_Success_Method_Implemented()
        {
            // Arrange
            var someService = new SomeService();
            var entity = new SomeEntity(Guid.NewGuid());
            // Act
            var result = someService.Create(entity);
            // Assert
            Assert.NotNull(result); /* => */ result.Should().NotBeNull();
            result.IsSuccessful.Should().BeTrue();
            // // Given
            // Give(x => new SomeService())
            //     // When
            //     .When(x => x.Create(new SomeEntity()))
            //     // Then
            //     .Then(x => x.Should().NotBeNull());
        }

        [Fact]
        public void ResultOfT_Should_Have_Parametrized_Success_Method_Implemented()
        {
            //Arrange
            var someService = new SomeService();
            var id = Guid.NewGuid();
            //Act
            var result = someService.Get(id);
            //Assert
            result.Should().NotBeNull();
            result.IsSuccessful.Should().BeTrue();
            result.Data.Should().NotBeNull();
            result.Data.Id.Should().Be(id);
        }

        [Fact]
        public void Result_Should_Have_Error_Method_Implemented()
        {
            //Arrange
            var someService = new SomeService();
            var entity = new SomeEntity(Guid.NewGuid());
            //Act
            var result = someService.CreateWithError(entity);
            //Assert
            result.Should().NotBeNull();
            result.IsSuccessful.Should().BeFalse();
            result.Exception.Should().BeAssignableTo<ArgumentNullException>();
        }

        [Fact]
        public void ResultOfT_Should_Have_Parametrized_Error_Method_Implemented()
        {
            //Arrange
            var someService = new SomeService();
            var id = Guid.NewGuid();
            //Act
            var result = someService.GetWithError(id);
            //Assert
            result.Should().NotBeNull();
            result.IsSuccessful.Should().BeFalse();
            result.Exception.Should().BeAssignableTo<Exception>();
            result.Data.Should().BeNull();
        }
    }
}