﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Collections_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            var ints = new List<int> { 1, 2, 3, 4, 56, 7, 8 };

            ints.Add(123);
            
            ints.AddRange(new []{ 1,2,3,4});

            var list = new PersonsList();

            foreach (Person person in list)
            {
                Console.WriteLine($"Name: {person.FirstName} {person.LastName}");
            }
            
            Console.WriteLine(list[0]);
            
            list.Add(new Person("", ""));

            var dict = new Dictionary<string, Func<int, int>>()
            {
                { "klucz", i => i * i }
            };

            if (dict.TryGetValue("klucz", out var func))
                Console.WriteLine($"Wynik to: {func(4)}");

            var queue = new Queue<int>();

            int res;
            queue.Enqueue(1);
            queue.Enqueue(2);
            res = queue.Dequeue(); // 1
            res = queue.Dequeue(); // 2
            Console.WriteLine($"Value from queue: {res}");

            var per = new Person("","");
        }
    }

    public class Person
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }

        private List<Role> _roles = new();

        public IReadOnlyList<Role> Roles => _roles.AsReadOnly(); 
        
        public Person(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public void AddRole(Role role, dynamic user)
        {
            if (user.IsAdmin())
            {
                _roles.Add(role);
            }
        }
    }

    public class Role {}
    
    public class PersonsList : IEnumerable
    {
        private readonly List<Person> _persons = new()
        {
            new Person ("John", "Doe")
        };

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new PersonsEnumerator(_persons.ToArray());
        }

        public Person this[int i]
        {
            get => _persons.ElementAt(i);
            set => _persons[i] = value;
        }

        public void Add(Person person)
            => _persons.Add(person);
        
        private class PersonsEnumerator : IEnumerator
        {
            private Person[] _persons;
            private int _position = -1;
            
            public PersonsEnumerator(Person[] persons)
            {
                _persons = persons;
            }
            
            public bool MoveNext()
            {
                _position++;
                return (_position < _persons.Length);
            }

            public void Reset() => _position = -1;

            public object Current => _persons[_position];
        }
    }
}