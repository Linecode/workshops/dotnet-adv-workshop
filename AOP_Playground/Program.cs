﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using PostSharp.Aspects;
using PostSharp.Patterns.Threading;
using PostSharp.Reflection;
using PostSharp.Serialization;

namespace AOP_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            // var myClass = new MyClass();
            // List<Task> tasks = new List<Task>();
            //
            // for (int i = 0; i < 100; i++)
            // {
            //     tasks.Add(Task.Run(() =>
            //     {
            //         for (int j = 0; j < 100; j++)
            //         {
            //             myClass.AddMyValue();
            //         }
            //     }));
            // }
            //
            // Task.WaitAll(tasks.ToArray());
            //
            // Console.WriteLine($"MyValue: {myClass.GetMyValue()}");

            Foo(1, 2);

            var attr = System.Attribute.GetCustomAttributes(typeof(Person));

            Console.WriteLine("");
        }

        [LoggingAspect]
        static int Foo(int a, int b)
        {
            Console.WriteLine("Hello World!");
            return 3;
        }
    }

    [AutoDataContract]
    public class Person
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class AutoDataContractAttribute : TypeLevelAspect, IAspectProvider
    {
        public IEnumerable<AspectInstance> ProvideAspects(object targetElement)
        {
            var targetType = (Type)targetElement;

            var introduceDataContractAspect = new CustomAttributeIntroductionAspect(
                new ObjectConstruction(typeof(DataContractAttribute).GetConstructor(Type.EmptyTypes)));

            var introduceDataMemberAspect = new CustomAttributeIntroductionAspect(
                new ObjectConstruction(typeof(DataMemberAttribute).GetConstructor(Type.EmptyTypes)));

            yield return new AspectInstance(targetType, introduceDataContractAspect);

            foreach (var property in targetType.GetProperties(BindingFlags.Public | BindingFlags.DeclaredOnly | BindingFlags.Instance))
            {
                if (property.CanWrite)
                {
                    yield return new AspectInstance(property, introduceDataMemberAspect);
                }
            }
        }
    }

    [PSerializable]
    public class LoggingAspect : OnMethodBoundaryAspect
    {
        public override void OnEntry(MethodExecutionArgs args)
        {
            Console.WriteLine("Method {0}({1}) started", args.Method.Name, string.Join(",", args.Arguments));
        }

        public override void OnSuccess(MethodExecutionArgs args)
        {
            Console.WriteLine("Method {0} started returned {1}", args.Method.Name, args.ReturnValue);
        }
    }

    // [Synchronized]
    [ReaderWriterSynchronized]
    class MyClass
    {
        private int myValue = 0;

        [Writer]
        public void AddMyValue()
        {
            myValue++;
        }

        [Reader]
        public int GetMyValue()
        {
            return myValue;
        }
    }
}