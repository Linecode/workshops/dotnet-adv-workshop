﻿using System;
using System.Threading;

namespace Multithread_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            // AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
            // {
            //     Console.WriteLine("Oh no! :(");
            // };
            //
            // var t1 = new Thread(() =>
            // {
            //     int i = 0;
            //     
            //     while (true)
            //     {
            //         DumpThread($"t1 {i}");
            //
            //         i++;
            //         
            //         Thread.Sleep(1000);
            //     }
            //
            //     // throw new NullReferenceException();
            // }) { IsBackground = true };
            //
            // var t2 = new Thread(() =>
            // {
            //     int i = 0;
            //     
            //     while (true)
            //     {
            //         DumpThread($"t2 {i}");
            //
            //         i--;
            //         
            //         Thread.Sleep(1000);
            //     }
            //
            //     // throw new NullReferenceException();
            // }) { IsBackground = true };
            //
            // t1.Start();
            // t2.Start();

            // var wait = new SpinWait();
            //
            // var t1 = new Thread(() =>
            // {
            //     var i = 0;
            //     while (true)
            //     {
            //         DumpThread(i.ToString());
            //         i++;
            //
            //         if (wait.NextSpinWillYield)
            //         {
            //             Console.WriteLine("Will yield, oh no, abort! ");
            //             break;
            //         }
            //         else
            //         {
            //             wait.SpinOnce();
            //         }
            //     }
            // }) { IsBackground = true };
            //
            // t1.Start();

            // using var cts = new CancellationTokenSource();
            //
            // cts.Token.Register(() =>
            // {
            //     Console.WriteLine("Before cancelled!!");
            // });
            //
            // cts.Token.Register(() =>
            // {
            //     Console.WriteLine("Before cancelled2!!");
            // });
            //
            // cts.Token.Register(() =>
            // {
            //     Console.WriteLine("Before cancelled3!!");
            // });
            //
            // var t1 = new Thread((cts) =>
            // {
            //     var token = (CancellationToken)cts;
            //
            //     var i = 0;
            //
            //     while (!token.IsCancellationRequested)
            //     {
            //         DumpThread(i.ToString());
            //         i++;
            //         Thread.Sleep(100);
            //     }
            //     
            //     Console.WriteLine("Cancelled!");
            // }){ IsBackground = true };
            //
            // t1.Start(cts.Token);
            //
            // Thread.Sleep(2000);
            //
            // cts.Cancel();

            // var t1 = new Thread(() =>
            // {
            //     while (true)
            //     {
            //         ThreadSafeCounter.Value++;
            //         Console.WriteLine($"t1 set counter: {ThreadSafeCounter.Value}");
            //         
            //         Thread.Sleep(200);
            //
            //         Console.WriteLine($"t1 counter: {ThreadSafeCounter.Value}");
            //     }
            // }) { IsBackground = true };
            //
            // var t2 = new Thread(() =>
            // {
            //     while (true)
            //     {
            //         Thread.Sleep(60);
            //         
            //         ThreadSafeCounter.Value++;
            //         Console.WriteLine($"t2 set counter: {ThreadSafeCounter.Value}");
            //         
            //         Thread.Sleep(140);
            //
            //         Console.WriteLine($"t2 counter: {ThreadSafeCounter.Value}");
            //     }
            // }) { IsBackground = true};
            //
            // t1.Start();
            // t2.Start();

            using var cts = new CancellationTokenSource();
            
            ThreadPool.QueueUserWorkItem(cts =>
            {
                var token = (CancellationToken)cts;
                int i = 0;
                while (!token.IsCancellationRequested)
                {
                    DumpThread(i.ToString());

                    i++;
                
                    Thread.Sleep(1000);
                }
            }, cts.Token);
        
            Thread.Sleep(2000);
            cts.Cancel();
            
            Console.ReadLine();
        }

        static void DumpThread(string label)
        {
            Console.WriteLine($"[{DateTime.Now:hh:mm:ss.fff}] {label}: " +
                              $"ID: {Thread.CurrentThread.ManagedThreadId} " +
                              $"IsOnThreadPool: {Thread.CurrentThread.IsThreadPoolThread}");
        }

        public static class ThreadSafeCounter
        {
            [ThreadStatic]
            public static int Value;
        }
    }
}