﻿using System;
using System.Threading;

namespace Synchronize_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            // Wykorzystując prywityw synchronizacyjny Monitor, spraw aby klasa BackAccount została klasą ThreadSafe
            // Pomyśl czy dałbyś radę opakować kod Minitor-a w klasę lub struktruę tak aby zamknąć kod "lock"-a w
            // klauzuli using
            // using (SomeCustom.Lock(obj, TimeSpan)) {
            //    // critical section
            // }

            var account = new BankAccount();
            
            // Napisz kod do testowania czy klasa BankAccount jest klasą ThreadSafe
            
            var t1 = new Thread((acc) =>
            {
                var ac = (BankAccount)acc;

                while (true)
                {
                    ac.Deposit(100);
                    
                    Thread.Sleep(1000);
                }
            }) { IsBackground = true};
            
            var t2 = new Thread((acc) =>
            {
                var ac = (BankAccount)acc;

                while (true)
                {
                    var amount = ac.Get();
                    
                    Console.WriteLine($"Amount: {amount}");
                    
                    Thread.Sleep(100);
                }
            }) { IsBackground = true};

            var t3 = new Thread((acc) =>
            {
                var ac = (BankAccount)acc;

                while (true)
                {
                    Thread.Sleep(2000);

                    ac.Withdraw(100);
                }
            });

            t1.Start(account);
            t2.Start(account);
            t3.Start(account);
        }
    }
    
    class BankAccount
    {
        private static object padlock = new object();
        private int balance;

        public void Deposit(int amount)
        {
            using (TimedLock.Lock(padlock, TimeSpan.FromSeconds(2)))
            {
                balance += amount;
                Console.WriteLine($"Deposit {amount}, current balance {balance}");
            }
        }

        public async void Withdraw(int amount)
        {
            using (TimedLock.Lock(padlock, TimeSpan.FromSeconds(2)))
            {
                balance -= amount;
                Console.WriteLine($"Withdraw {amount}, current balance {balance}");
            }
        }

        public int Get()
        {
            using (TimedLock.Lock(padlock, TimeSpan.FromSeconds(2)))
            {
                return balance;
            }
        }
    }
    
    public ref struct TimedLock
    {
        private object _target;

        private TimedLock(object o) => _target = o;

        public static TimedLock Lock(object o, TimeSpan timeout)
        {
            TimedLock t1 = new TimedLock(o);

            if (!Monitor.TryEnter(o, timeout))
            {
                throw new TimeoutException();
            }

            return t1;
        }

        public void Dispose() => Monitor.Exit(_target);
    }
}