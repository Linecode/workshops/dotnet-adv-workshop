# Delegates 3_1

- Zaimplementuj metodę rozszerzającą typ `Repository` o nazwię `Foreach`
- Metoda ma przyjmować 4 delegaty
  - action -> akcja do wykonania na każdym elemencie
  - beforeElement -> delegat do wykonania przed "akcją"
  - afterElement -> delegat do wykonania po "akcji"
  - filterElement -> delegat pozwalający na ograniczenie ilości elementów, które zostaną wziętę pod uwagę

Wymagania: 

- Wykorzystaj minimum dwa z dwa możliwych struktór do których można przypisać delegat (Action, Func, Predicate)
- delegaty beforeElement, actionElement oraz filter powinny być opcjonalne, 
- Wykorzystaj możliwosć nadawania labelek parametrom wywołania funkcji
- W metodzie `Main` w pliku `Program.cs` przetestuj różne możliwe ustawienia tego rozszerzenia