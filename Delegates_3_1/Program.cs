﻿using System;
using Delegates_3_1.Domain;
using Delegates_3_1.Infrastructure;

namespace Delegates_3_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var repository = new Repository();

            repository.Foreach(
                action: x => Console.WriteLine(x),
                beforeElement: _ => Console.WriteLine("-----------b--------------"),
                afterElement: _ => Console.WriteLine("-------------a-------------"),
                filterElement: x => x.Title.Contains("e"));

            

            repository.Filter(x => x.Title.Contains("f"));
        }
    }

    public static class RepositoryExtensions
    {
        public static void Foreach(
            this Repository repo,
            Action<Movie> action,
            Action<Movie> beforeElement = null,
            Action<Movie> afterElement = null,
            Predicate<Movie> filterElement = null)
        {
            foreach (var movie in repo.GetAll())
            {
                if (filterElement is null || filterElement?.Invoke(movie) == false)
                {
                    beforeElement?.Invoke(movie);
                    action(movie);
                    afterElement?.Invoke(movie);
                }
            }
        }
    }
}