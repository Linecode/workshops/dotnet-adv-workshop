﻿using System;
using System.Threading;

namespace MultiThread_8_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            // Wykorzystaj api ThreadPool aby: 
            // Stworzyć dwa wątki
            // Pierwszy z nich niech odlicza co sekundę w dół
            // Natomiast durgi z nich niech odlicza co sekundę w górę
            // W momencie kiedy pierwszy wątek osiągnie liczbę -10 zakończ wątek
            // W momencie kiedy drugi wątek osiągnie liczbę 15 zakończ wątek

            ThreadPool.QueueUserWorkItem(_ =>
            {
                int i = 0;
                while (i > -10)
                {
                    DumpThreadInfo();
                    i--;
                    Console.WriteLine($"t1: {i}");
                    Thread.Sleep(1000);
                }
            });

            ThreadPool.QueueUserWorkItem(_ =>
            {
                int i = 0;
                while (i < 15)
                {
                    DumpThreadInfo();
                    i++;
                    Console.WriteLine($"t2: {i}");
                    Thread.Sleep(1000);
                }
            });
            
            Console.ReadLine();
        }
        
        static void DumpThreadInfo()
        {
            Console.WriteLine($"Thread: id: {Thread.CurrentThread.ManagedThreadId} " +
                              $"isThreadPoolThread: {Thread.CurrentThread.IsThreadPoolThread} " +
                              $"isBackground: {Thread.CurrentThread.IsBackground}");
        }
    }
}