﻿using System;

namespace Extensions_Playground
{
    public class Program
    {
        static void Main(string[] args)
        {
            var person = new Person();
            
            person.Should();

            var obj = new object();
            
            obj.Should();
            
            "dasdas".Should();
        }
    }

    public class Person
    {
        
    }

    public static class ProgramExtensions
    {
        public static string GetName(this Program _)
        {
            return nameof(Program);
        }
    }

    public static class IntExtensions
    {
        public static bool IsOdd(this int source)
            => source % 2 == 1;

        public static bool IsEven(this int source)
            => !IsOdd(source);
    }

    public static class FluentAsserstions
    {
        public static void Should(this object source)
        {
            
        }
    }
}