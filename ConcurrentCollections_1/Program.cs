﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace ConcurrentCollections_1
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // wykorzystując więdze zdobytą podczas tych warsztatów napisz kod, który będzie działał na zasadzie producer / consumer
            // uzyj ConcurrentQueue<Action<int>> aby przechować kolejkę zadań
            
            Console.WriteLine("Hello World!");

            var cq = new ConcurrentQueue<Action<int>>();

            Action<int> workload = async (id) =>
            {
                await Task.Delay(100);
                Console.WriteLine($"Action handled by task: {id}");
            };
            
            cq.Enqueue(workload);

            await Task.Run(() =>
            {
                for (int i = 0; i < 500; i++)
                {
                    cq.Enqueue(workload);
                }
                
                Console.WriteLine("Added workloads!");
            });
            
            // .... implementacja
            
            Action consumer = () =>
            {
                while (cq.TryDequeue(out var task))
                {
                    task(Task.CurrentId ?? 0);
                }
            };
            
            var tasks = new Task[4];
            for (int i = 0; i < tasks.Length; i++)
                tasks[i] = Task.Factory.StartNew(consumer);

            await Task.WhenAll(tasks);

            Console.ReadLine();
        }
    }
}