﻿using System;

namespace Generics_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            var data = new DataSet<Test>();
            
            // data.SetData(string.Empty);
            
            // data.SetData(new Test());
            // var testobj = data.GetData();
            
            // var data2 = new DataSet<int>();
            Console.WriteLine("");
            
            GenericCounter<string>.Increment();
            GenericCounter<string>.Increment();
            GenericCounter<string>.Display();
            
            GenericCounter<int>.Increment();
            GenericCounter<int>.Display();
            GenericCounter<int>.Increment();
            GenericCounter<int>.Display();
        }
    }

    class GenericCounter<T>
    {
        private static int value;

        static GenericCounter()
        {
            Console.WriteLine("Initializing counter for {0}", typeof(T));
        }

        public static void Increment()
        {
            value++;
        }

        public static void Display()
        {
            Console.WriteLine("Counter for {0}:{1}", typeof(T), value);
        }
    }

    public class C
    {
        public void Method() {}
    }

    public class Test
    {
        public void A<T>(T param) where T : C
        {
            param.Method();
        }
    }

    public class DataSet<T> where T : class, new()
    {
        private T _data;

        public bool HasValue { get; private set; } = false;

        public void SetData(T data)
        {
            _data = data;
            HasValue = true;
        }

        public T GetData()
        {
            if (HasValue) return _data;
            // return default; // Data<int> => 0, Data<TestClass> => null
            return new T();
        }
    }

    public partial class D
    {
        public override string ToString()
        {
            string text = "original";
            CustomizeToString(ref text);
            return text;
        }

        partial void CustomizeToString(ref string text);
    }

    public partial class D
    {
        partial void CustomizeToString(ref string text)
        {
            text += " customize!";
        }
    }
}