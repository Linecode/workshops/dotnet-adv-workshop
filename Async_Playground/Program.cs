﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace Async_Playground
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // Console.WriteLine("Hello World!");
            // var t = Task.Delay(5000);
            // t.Wait();
            // Console.WriteLine("Hello World!");
            
            // DumpThread("Main");
            
            // Legacy 
            // var t = new Task(() =>
            // {
            //     DumpThread("T1");
            //
            //     Task.Delay(100).Wait();
            // });
            // t.Start();
            // var t = Task.Factory.StartNew(() =>
            // {
            //     DumpThread("T1");
            //     
            //     Task.Delay(100).Wait();
            //     // Thread.Sleep(100); -> Please don;t ;( 
            // });
            //
            // t.Wait();

            // JS: Promise
            //  Promise.then(() => {})

            // TaskScheduler.UnobservedTaskException += (sender, eventArgs) =>
            // {
            //     Console.WriteLine("unhandled task error");
            //     eventArgs.SetObserved();
            // };
            //
            // Task<int> t = Task.Run(() =>
            // {
            //     DumpThread("T1");
            //
            //     Task.Delay(2000).Wait();
            //
            //     // throw new ArgumentNullException();
            //
            //     // Task.Yield(); // wymusza context switching
            //     return 1; 
            // });

            // t.ContinueWith((x) =>
            // {
            //     Console.WriteLine($"Continue with {x}");
            //     DumpThread("Con");
            //     Task.Delay(100).Wait();
            // });

            // Thread.Sleep(1000);
            //
            // GC.Collect(2, GCCollectionMode.Forced, true);
            // GC.WaitForPendingFinalizers();
            // GC.Collect(2, GCCollectionMode.Forced, true);
            
            // try
            // {
            // var result = t.Result;
            // }
            // catch (AggregateException ex)
            // {
            //     Console.WriteLine("Handling error!");
            // }

            // var parent = Task.Factory.StartNew(() =>
            // {
            //     Console.WriteLine("Running parent task");
            //
            //     var child = Task.Factory.StartNew(() =>
            //     {
            //         Console.WriteLine("Running child started");
            //         Thread.Sleep(2000);
            //         Console.WriteLine("Running child ended");
            //     }, TaskCreationOptions.AttachedToParent);
            // });
            //
            // parent.Wait();
            //
            // Console.WriteLine("The End");

            // DumpThread("Main");

            // var tasks = new List<Task>();
            // for (var i = 0; i < 10; i++)
            // {
            //     int temp = i;
            //     tasks.Add(Task.Run(() =>
            //     {
            //         Task.Delay(1000 * temp).Wait();
            //         Console.WriteLine($"Ended task id: {temp}");
            //     }));
            // }
            //
            // Task.WaitAny(tasks.ToArray());
            //

            // Task<int> t = Task.Run(async () =>
            // {
            //     int i = 0;
            //     while (i < 10)
            //     {
            //         Console.WriteLine("Hello from task");
            //
            //         await Task.Delay(1000);
            //
            //         i++;
            //     }
            //
            //     return i;
            // });
            //
            // Console.WriteLine("The End");
            //
            // var result = await t;

            // Task task = Sleep();
            //
            // DumpThread("Before first await");
            //
            // await task;
            //
            // DumpThread("Before second await");
            //
            // await task;
            //
            // DumpThread("The End");
            //

            // await TimeSpan.FromSeconds(30);

            // try
            // {
            //     await GetSmthAsync();
            // }
            // catch (Exception e)
            // {
            //     // Handle errors from GetSyncWithAsync
            //     // Handle errors from GetSmthAsync
            // }

            // Action action = async () =>
            // {
            //     Console.WriteLine($"Task creaetd: {Task.CurrentId}");
            //     DumpThread($"TaskId: {Task.CurrentId}");
            //     await Task.Delay(1000);
            //     // Console.WriteLine($"Task ended: {Task.CurrentId}");
            // };
            //
            // Task[] tasks = new Task[4];
            // for (int i = 0; i < tasks.Length; i++)
            //     tasks[i] = Task.Factory.StartNew(action);
            //
            // await Task.WhenAll(tasks);

            // var tasks = Enumerable.Range(0, 10)
            //     .Select(i =>
            //     {
            //         Console.WriteLine(i);
            //         return Task.Delay(3000);
            //     }).ToList();
            //
            // foreach (var task in tasks)
            // {
            //     // 1s - 1task
            //     // ---- 
            //     // zakonczone
            //     await task;
            // }

            
            Console.ReadLine();
        }

        public static async ValueTask<int> GetSmthAsync(int x)
        {
            if (x < 10) return 10;
            
            return await GetSmthAsync();
        }

        public static Task<int> GetSyncWithAsync()
        {
            return Task.FromResult(10);
        }

        public static Task<int> GetSmthAsync()
        {
            return GetSyncWithAsync();
        }

        public static async Task Sleep()
        {
            DumpThread("Sleep started");

            await Task.Delay(1000);
            
            DumpThread("Sleep ended");
        }

        static void DumpThread(string label)
        {
            Console.WriteLine($"[{DateTime.Now:hh:mm:ss.fff}] {label}: " +
                              $"ID: {Thread.CurrentThread.ManagedThreadId} " +
                              $"IsOnThreadPool: {Thread.CurrentThread.IsThreadPoolThread} " +
                              $"IsBackground: {Thread.CurrentThread.IsBackground}");
        }
    }
    
    public static class TimeSpanExtensions
    {
        public static TaskAwaiter GetAwaiter(this TimeSpan timeSpan)
        {
            return Task.Delay(timeSpan).GetAwaiter();
        }
    }
}