﻿using System;
using System.Threading;

namespace Async_9_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            // Za pomocą api Task.Run, stwórz dwa wątki background
            // Pierwszy z nich niech odlicza co sekundę w dół
            // Natomiast durgi z nich niech odlicza co sekundę w górę
            // W momencie kiedy pierwszy wątek osiągnie liczbę -10 zakończ wątek
            // W momencie kiedy drugi wątek osiągnie liczbę 15 zakończ wątek
        }
        
        static void DumpThreadInfo()
        {
            Console.WriteLine($"Thread: id: {Thread.CurrentThread.ManagedThreadId} " +
                              $"isThreadPoolThread: {Thread.CurrentThread.IsThreadPoolThread} " +
                              $"isBackground: {Thread.CurrentThread.IsBackground}");
        }
    }
}