﻿using System;
using System.Threading;

namespace Synchronize_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            // Wykorzystując prywityw synchronizacyjny lock, spraw aby klasa BackAccount została klasą ThreadSafe

            var account = new BankAccount();
            
            // Napisz kod do testowania czy klasa BankAccount jest klasą ThreadSafe
            var t1 = new Thread((acc) =>
            {
                var ac = (BankAccount)acc;

                while (true)
                {
                    ac.Deposit(100);
                    
                    Thread.Sleep(1000);
                }
            }) { IsBackground = true};
            
            var t2 = new Thread((acc) =>
            {
                var ac = (BankAccount)acc;

                while (true)
                {
                    var amount = ac.Get();
                    
                    Console.WriteLine($"Amount: {amount}");
                    
                    Thread.Sleep(100);
                }
            }) { IsBackground = true};

            var t3 = new Thread((acc) =>
            {
                var ac = (BankAccount)acc;

                while (true)
                {
                    Thread.Sleep(2000);

                    ac.Withdraw(100);
                }
            });

            t1.Start(account);
            t2.Start(account);
            t3.Start(account);
            
            Console.ReadLine();
        }
    }

    class BankAccount
    {
        private static object padlock = new object();
        private int balance;

        public void Deposit(int amount)
        {
            lock (padlock)
            {
                balance += amount;
                Console.WriteLine($"Deposit {amount}, current balance {balance}");
            }
        }

        public async void Withdraw(int amount)
        {
            lock (padlock)
            {
                balance -= amount;
                Console.WriteLine($"Withdraw {amount}, current balance {balance}");
            }
        }

        public int Get()
        {
            lock (padlock)
            {
                return balance;
            }
        }
    }
}