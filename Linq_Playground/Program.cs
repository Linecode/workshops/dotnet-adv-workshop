﻿using System;
using System.Linq;
using Linq_Playground.Domain;
using Linq_Playground.Infrastructure;

namespace Linq_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            var repository = new Repository();

            // var movieTitles = from movie in repository.GetAll() select movie.Title;

            var movieTitles = repository
                .GetAll()
                // .Select(x => x.Title)
                // .First(x => x.Contains("e"));
                .SelectMany(x => x.Cast)
                // .Select(x => $"{x.FirstName} {x.LastName} : {x.Role} ")
                // .OrderBy(x => x)
                // .Skip(1)
                // .Take(repository.GetAll().SelectMany(x => x.Cast).Count() - 2)
                .Select(x => new { x.FirstName, x.LastName })
                .ToList();
                // .Aggregate(string.Empty, (c, n) => $"{c} \n {n}")
                // .Count();

            Console.WriteLine(movieTitles);
            
            // first -> return first element
            // single -> sequence contains more than one element

            // foreach (var title in movieTitles)
            // {
            //     Console.WriteLine(title);
            // }
        }
    }
}