﻿using System;
using System.Collections.Generic;
using System.Reflection;
using IoC_Microsfot_Lib;
using IoC_Microsoft_Common;
using IoC_Microsoft_Lib2;
using Microsoft.Extensions.DependencyInjection;

namespace IoC_Microsoft_Simple
{
    class Program
    {
        static void Main(string[] args)
        {
            // ------- Composition Root
            var services = new ServiceCollection();

            services.AddScoped<SomeType>();

            services.AddTransient<Test1>();
            // services.AddTransient<Test2>();

            // services.AddTransient<IMarker, A>();
            // services.AddTransient<IMarker, B>();
            // services.AddTransient<IMarker, C>();
            //
            // services.AddTransient<IB, B>();

            services.Scan(scan => scan.FromCallingAssembly()
                .AddClasses(@class => @class.AssignableTo<IMarker>())
                    .AsImplementedInterfaces()
                    .WithTransientLifetime());

            services.Scan(scan => scan.FromAssemblyOf<Repo2>()
                .AddClasses(@class => @class.AssignableTo<IRepository>())
                .AsImplementedInterfaces());

            services.AddSingleton<S>();

            // services.AddScoped<Startup>();

            services.AddSuperFancy(options =>
            {
                options.OtherConfiguration = "AA";
                options.SomeConfiguration = "BB";
            });
            
            /// -----------------------

            using var provider = services.BuildServiceProvider();
            using var scope = provider.CreateScope();

            var repo = scope.ServiceProvider.GetRequiredService<IRepository>();
            var someType = scope.ServiceProvider.GetRequiredService<SomeType>();
            var b = scope.ServiceProvider.GetRequiredService<IB>();
            var c = scope.ServiceProvider.GetRequiredService<IMarker>();
            var list = scope.ServiceProvider.GetService<IEnumerable<IMarker>>();
            var fancy = scope.ServiceProvider.GetRequiredService<SuperFancyReg>();
        }
    }

    public class S
    {
    }

    public class SomeType
    {
        private readonly IRepository _repository;

        public SomeType(IRepository repository)
        {
            _repository = repository;
        }
    }

    public interface IMarker {}

    public class A : IMarker { }
    
    public interface IB {}
    public class B : IMarker, IB { }
    public class C : IMarker { }
    
    public class D : IMarker {}

    public class SuperFancyReg
    {
        public SuperFancyOptions Options { get; set; }
    }

    public class SuperFancyOptions
    {
        public string SomeConfiguration { get; set; }
        public string OtherConfiguration { get; set; }
    }

    public static class ServiceCollectionExtensions
    {
        public static ServiceCollection AddSuperFancy(this ServiceCollection services, Action<SuperFancyOptions> configuration)
        {
            var options = new SuperFancyOptions();
            configuration(options);

            services.AddSingleton(new SuperFancyReg { Options = options });

            return services;
        }
    }
}