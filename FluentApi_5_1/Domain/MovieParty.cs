using System;

namespace FluentApi_5_1.Domain
{
    public class MovieParty
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public MoviePartyRole Role { get; set; }
    }
}