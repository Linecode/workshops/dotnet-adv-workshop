using System;
using System.Collections.Generic;
using System.Linq;

namespace FluentApi_5_1.Domain
{
    public class Movie
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public MovieParty Director { get; set; }
        public List<MovieParty> Cast { get; set; } = new ();

        public override string ToString()
        {
            return $"MovieId: {Id} \nTitle: {Title} \nDirector: {Director.FirstName} {Director.LastName} \n" +
                   $"Cast: {Cast?.Select(x => $"  {x.Role}: {x.FirstName} {x.LastName}").Aggregate(string.Empty, (c, n) => $"{c} \n{n}")}";
        }
    }
}