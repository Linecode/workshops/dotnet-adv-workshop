# FluentApi 5_1

- Zaimplementuj wzorzec projektowy Builder tak aby można było zbudować obiekt klasy Movie za pomocą składni: 

```csharp
var movie = new MovieBuilder()
    .WithId(Guid.NewId())
    .WithTitle("dsadsa")
    .WithDirector(new MovieParty())
    .WithCastMember(new MovieParty())
```

# FluentApi 5_1* (dla chętnych)

Za pomocą zagnieżdzonych builder-ów spróbuj osiągnąć ponizszą składnię: 

```csharp
var movie = new MovieBuilder()
    .Id(Guid.NewGuid())
    .Title("dsadsaa")
    .Directed
        .Id(Guid.NewGuid())
        .FullName("dsadasdsa", "dsadsadas")
    .Performed
        .Id(Guid.NewGuid())
        .FullName("dsadsadsadas", "dsadsadsaas")
        .Add()
    .Performed
        .Id(Guid.NewGuid())
        .FullName("das", "dsa")
        .Add()
    .Build();
```
