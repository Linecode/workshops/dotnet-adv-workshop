﻿using System;
using System.Threading;

namespace MultiThread_8_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            // Wykorzystaj strukturę SpinWait aby wyjść z pętli przed wykonaniem context switch przez procesor
            var wait = new SpinWait();
            var t = new Thread(() =>
            {
                while (true)
                {
                    DumpThreadInfo();
                    if (wait.NextSpinWillYield) break;
                    else wait.SpinOnce();
                }
            }) { IsBackground = true };
            
            t.Start();

            Console.ReadLine();
        }
        
        static void DumpThreadInfo()
        {
            Console.WriteLine($"Thread: id: {Thread.CurrentThread.ManagedThreadId} " +
                              $"isThreadPoolThread: {Thread.CurrentThread.IsThreadPoolThread} " +
                              $"isBackground: {Thread.CurrentThread.IsBackground}");
        }
    }
}