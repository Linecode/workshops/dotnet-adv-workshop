﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using Extensions_1_1.Commands;
using Extensions_1_1.Domain;
using Extensions_1_1.Handlers;
using Extensions_1_1.Infrastructure;
using Extensions_1_1.Queries;

namespace Extensions_1_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var repository = new Repository();
            
            #region DisplayRepository 
            Console.WriteLine("Generated Repository");
            foreach (var mov in repository.GetAll())
                Console.WriteLine(mov);
            #endregion
            

            var movieId = Guid.NewGuid();
            var createMovieCommand = new CreateMovieCommand(movieId ,"Lorem", new MovieParty
            {
                Id = Guid.NewGuid(),
                Role = MoviePartyRole.Director,
                FirstName = "Lorem",
                LastName = "Lipsum",
            }, null);
            
            //createMovieCommand.BeforeHandle = c => Console.WriteLine(JsonSerializer.Serialize(c));
            
            var createMovieHandler = new CreateMovieHandler(repository);
            createMovieHandler.Handle(createMovieCommand);

            var getMovieQuery = new GetMovieQuery(movieId);
            var queryHandler = new GetMovieQueryHandler(repository);

            var movie = queryHandler.Handle(getMovieQuery);

            var data = movie.Match(
                OnSuccess: data => data,
                OnError: error => throw error // No happy path programming :( 
                );

            Console.WriteLine(movie);
        }
    }
}