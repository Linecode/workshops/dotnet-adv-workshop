namespace Extensions_1_1
{
    public interface IQuery<TResult>
    {
        
    }

    public interface IQueryHandler<T, TResult> where T : IQuery<TResult>
    {
        public Result<TResult> Handle(T query);
    }
}