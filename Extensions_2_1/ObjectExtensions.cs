namespace Extensions_1_1
{
    public static class ObjectExtensions
    {
        public static Result<T> AsResult<T>(this object source)
        {
            return Result<T>.Success((T) source);
        }
    }
}