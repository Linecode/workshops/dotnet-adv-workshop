using Extensions_1_1.Domain;
using Extensions_1_1.Infrastructure;
using Extensions_1_1.Queries;

namespace Extensions_1_1.Handlers
{
    public class GetMovieQueryHandler : IQueryHandler<GetMovieQuery, Movie>
    {
        private readonly Repository _repository;

        public GetMovieQueryHandler(Repository repository)
        {
            _repository = repository;
        }
        
        public Result<Movie> Handle(GetMovieQuery query)
        {
            var movie = _repository.Get(query.MovieId);

            return movie.AsResult<Movie>();
        }
    }
}