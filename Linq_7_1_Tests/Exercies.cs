using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Linq_7_1;
using Linq_7_1.Domain;
using Xunit;

namespace Linq_7_1_Tests
{
    public class Person
    {
        public decimal Pay()
        {
            return default;
        }

        public Person Born()
        {
            return default;
        }
    }

    public abstract class PersonSpec
    {
        public class When_Paying : PersonSpec
        {
            [Fact]
            public void Should_Smth() { }
            
            [Fact]
            public void Shoudl_Smth_Else() {}
        }

        public class When_Borning : PersonSpec
        {
            [Fact]
            public void Should_Smth() { }
            
            [Fact]
            public void Shoudl_Smth_Else() {}
        }
    }
    
    public abstract class AllExercies
    {
        public class Exercise_1 : AllExercies
        {
            [Theory]
            [InlineData(0, null)]
            [InlineData(0, "")]
            [InlineData(0, "e", "e")]
            [InlineData(1, "eas", "e")]
            [InlineData(1, "e", "eas")]
            [InlineData(0, "eeeeeeeeeeeeee", "eeeeeeeeeeeeee")]
            [InlineData(3, "ease", "easded", "esasa", "e", "e")]
            [InlineData(2, "eeeeeee", "eeeeeee", "", "e", "eeeeeeee")]
            public void RunTests(int expectedResult, params string[] words)
            {
                var result = Exercies.Exercise_1(words?.ToList());

                result.Should().Be(expectedResult);
            }

            [Fact]
            public void RunTest2()
            {
                var result = Exercies.Exercise_1(new List<string>() { 5.ToString()});
                
                result.Should().Be(5);
            }
        }

        public class Exercise_2 : AllExercies
        {
            [Theory]
            [InlineData(0, "", "", "")]
            [InlineData(0, "dsDScx", "", "dsaWDAScxz")]
            [InlineData(4, "dsDScx", "ddD", "dsaWDAScxz")]
            [InlineData(7, "dsDScx", "ddDdD", "dsaWDAScxz")]
            [InlineData(10, "dsDScx", "ddDdD", "ddd", "dsaWDAScxz")]
            public void RunTests(int expectedResult, params string[] words)
            {
                var result = Exercies.Exercise_2(words.ToList());

                result.Should().Be(expectedResult);
            }
        }

        public class Exercise_3 : AllExercies
        {
            [Theory]
            [InlineData("The last word is ze", "dssa",  "ze", "dedds", "cvm,mncv")]
            [InlineData("The last word is zze", "zze", "ze")]
            [InlineData("The last word is ze", "ze")]
            public void RunTests(string expectedResult, params string[] words)
            {
                var result = Exercies.Exercise_3(words.ToList());

                result.Should().Be(expectedResult);
            }
        }

        public class Exercise_4 : AllExercies
        {
            
            [Fact]
            public void RunTests()
            {
                var movies = new List<Movie>()
                {
                    new() { Id = Guid.NewGuid(), Title = " adsadadsa " },
                    new() { Id = Guid.NewGuid(), Title = "dsa " },
                    new() { Id = Guid.NewGuid(), Title = "dsasadadsa " },
                    new() { Id = Guid.NewGuid(), Title = "Msadadsa vol 2" },
                    new() { Id = Guid.NewGuid(), Title = "adsadadsa " },
                    new() { Id = Guid.NewGuid(), Title = "Msadadsa vol 2" },
                };

                var result = Exercies.Exercise_4(movies);

                result.Should().NotBeNull();
                result.Id.Should().Be(movies.ElementAt(3).Id);
                result.Title.Should().Be(movies.ElementAt(3).Title);
            }
        }

        public class Exercise_5 : AllExercies
        {
            [Fact]
            public void RunTest()
            {
                int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 23 };
                string[] words =
                    { "a", "b", "c", "dsa", "dsa", "cxz", "csa", "cxz", "dsa", "cxz", "cxz", "cxz", "fds" };

                var result = Exercies.Exercise_5(numbers, words);

                result.Should().NotBeEmpty()
                    .And.HaveCount(13)
                    .And.Contain("1 - a")
                    .And.Contain("2 - b")
                    .And.Contain("3 - c")
                    .And.Contain("4 - dsa")
                    .And.Contain("6 - cxz")
                    .And.Contain("7 - csa");
            }
        }

        public class Exercise_7 : AllExercies
        {
            [Theory]
            [InlineData("a&b", "a", "b")]
            [InlineData("a&b&c&d&ee", "a", "b", "c", "d", "ee")]
            public void RunTest(string expected, params string[] words)
            {
                var result = Exercies.Exercise_6(words);
                result.Should().Be(expected);
            }
        }
    }
}