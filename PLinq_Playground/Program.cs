﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using BenchmarkDotNet.Toolchains.InProcess.NoEmit;

namespace PLinq_Playground
{

    using System;
    using System.Security.Cryptography;
    using BenchmarkDotNet.Attributes;
    using BenchmarkDotNet.Running;

    namespace MyBenchmarks
    {
        public class Md5VsSha256
        {
            // private const int N = 10000;
            // private readonly byte[] data;
            //
            // private readonly SHA256 sha256 = SHA256.Create();
            // private readonly MD5 md5 = MD5.Create();
            //
            // public Md5VsSha256()
            // {
            //     data = new byte[N];
            //     new Random(42).NextBytes(data);
            // }
            //
            // [Benchmark]
            // public byte[] Sha256() => sha256.ComputeHash(data);
            //
            // [Benchmark]
            // public byte[] Md5() => md5.ComputeHash(data);
            
            private IEnumerable<int> source = Enumerable.Range(0, 100_000).ToList();
            
            // [Benchmark]
            // public string AsParallel()
            // {
            //     var evenNums = source.AsParallel()
            //         .Where(x => x % 2 == 0)
            //         .Select(x => x);
            //
            //     return $"{evenNums.Count()} even numbers out of {source.Count()} total";
            // }
            //     
            // [Benchmark]
            // public string AsParallel2()
            // {
            //     var evenNums = source.AsParallel().WithDegreeOfParallelism(2)
            //         .Where(x => x % 2 == 0)
            //         .Select(x => x);
            //
            //     return $"{evenNums.Count()} even numbers out of {source.Count()} total";
            // }
            //     
            // [Benchmark]
            // public string AsParallel4()
            // {
            //     var evenNums = source.AsParallel().WithDegreeOfParallelism(4)
            //         .Where(x => x % 2 == 0)
            //         .Select(x => x);
            //
            //     return $"{evenNums.Count()} even numbers out of {source.Count()} total";
            // }
            //     
            // [Benchmark]
            // public string Seq()
            // {
            //     var evenNums = source
            //         .Where(x => x % 2 == 0)
            //         .Select(x => x);
            //
            //     return $"{evenNums.Count()} even numbers out of {source.Count()} total";
            // }
            
            [Benchmark]
            public IList<int> GetPrimePLinq() => source.AsParallel().Where(IsPrime).ToList();

            [Benchmark]
            public IList<int> GetPrime() => source.Where(IsPrime).ToList();

            [Benchmark]
            public IList<int> GetPrimeListWithParallel()
            {
                var primeNumbers = new ConcurrentBag<int>();

                Parallel.ForEach(source, i =>
                {
                    if (IsPrime(i))
                    {
                        primeNumbers.Add(i);
                    }
                });

                // Action action = () => { };
                //
                // Parallel.Invoke(action,action,action,action,action);

                return primeNumbers.ToList();
            }

            private static bool IsPrime(int number)
            {
                if (number < 2)
                {
                    return false;
                }

                for (var divisor = 2; divisor <= Math.Sqrt(number); divisor++)
                {
                    if (number % divisor == 0)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public class Program
        {
            public static void Main(string[] args)
            {
                var summary = BenchmarkRunner.Run(typeof(Program).Assembly);
            }
        }
    }
    
    // class Program
    // {
    //     static void Main(string[] args)
    //     {
    //         var summary = BenchmarkRunner.Run(typeof(Program).Assembly);
    //
    //         Console.ReadLine();
    //     }
    // }
    //
    // class Mmonodsa
    // {
    //     private IEnumerable<int> source = Enumerable.Range(0, 10_000).ToList();
    //
    //     [Benchmark]
    //     public string AsParallel()
    //     {
    //         var evenNums = source.AsParallel()
    //             .Where(x => x % 2 == 0)
    //             .Select(x => x);
    //
    //         return $"{evenNums.Count()} even numbers out of {source.Count()} total";
    //     }
    //         
    //     [Benchmark]
    //     public string AsParallel2()
    //     {
    //         var evenNums = source.AsParallel().WithDegreeOfParallelism(2)
    //             .Where(x => x % 2 == 0)
    //             .Select(x => x);
    //
    //         return $"{evenNums.Count()} even numbers out of {source.Count()} total";
    //     }
    //         
    //     [Benchmark]
    //     public string AsParallel4()
    //     {
    //         var evenNums = source.AsParallel().WithDegreeOfParallelism(4)
    //             .Where(x => x % 2 == 0)
    //             .Select(x => x);
    //
    //         return $"{evenNums.Count()} even numbers out of {source.Count()} total";
    //     }
    //         
    //     [Benchmark]
    //     public string Seq()
    //     {
    //         var evenNums = source
    //             .Where(x => x % 2 == 0)
    //             .Select(x => x);
    //
    //         return $"{evenNums.Count()} even numbers out of {source.Count()} total";
    //     }
    // }
}