﻿using System;
using System.Threading.Tasks;

namespace Async_9_0
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // Wykorzystując api Task.Run stwórz wątek, który będzie po sekunde wypisywał na konsole
            // obecną datę bez blokowania możliwości pisania w konsoli. 

            Console.ReadLine();
        }
    }
}