﻿using System;
using System.Threading;

namespace MultiThread_8_Sync
{
    class Program
    {
        private static int count = 0;
        static void Main(string[] args)
        {
            // Wykorzsytując klasę ReaderWriterLockSlim zmodyfikuj klasę BankAccount tak aby była 
            // Thread Safe
            // Sprawdz jakie będzie zachowanie programu, kiedy utworzysz np. 5 wątków wpłacających na konto
            
            var bankAccount = new BankAccount();
            
            var t1 = new Thread((account) =>
            {
                var acc = (BankAccount)account;
                
                while (true)
                {
                    acc.Deposit(100);
                
                    Thread.Sleep(1000);
                }
            }) { IsBackground = true };

            var t2 = new Thread((account) =>
            {
                var acc = (BankAccount)account;

                while (true)
                {
                    var amount = acc.Get();

                    Console.WriteLine($"Amount: {amount}");
                    
                    Thread.Sleep(100);
                }
            }) { IsBackground = true };

            t1.Start(bankAccount);
            t2.Start(bankAccount);
        }
    }
    
    class BankAccount
    {
        private int balance;

        public void Deposit(int amount)
        {
            balance += amount;
            Console.WriteLine($"Deposit {amount}, current balance {balance}");
        }

        public void Withdraw(int amount)
        {
            balance -= amount;
        }

        public int Get()
        {
            return balance;
        }
    }
}