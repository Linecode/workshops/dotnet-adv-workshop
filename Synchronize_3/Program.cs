﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nito.AsyncEx;

namespace Synchronize_3
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            // Wykorzystując klasę AsyncLock z biblioteki AsyncEx, spraw aby klasa BackAccount została klasą ThreadSafe

            var account = new BankAccount();
            
            // Napisz kod do testowania czy klasa BankAccount jest klasą ThreadSafe

            Action actionDeposit = async () =>
            {
                while (true)
                {
                    await account.Deposit(100);
                    await Task.Delay(1000);
                }
            };
            
            Action actionWith = async () =>
            {
                await Task.Delay(15);
                
                while (true)
                {
                    await account.Withdraw(10);
                    await Task.Delay(1000);
                }
            };

            Action actionGet = async () =>
            {
                await Task.Delay(15);

                while (true)
                {
                    var amount = await account.Get();
                    Console.WriteLine($"Amount: {amount}");
                    await Task.Delay(1000);
                }
            };
            
            var tasks = new Task[10];
            for (int i = 0; i < tasks.Length - 1; i++)
            {
                var rand = new Random().Next(1, 3);
                if (rand % 2 == 0)
                    tasks[i] = Task.Factory.StartNew(actionDeposit, TaskCreationOptions.LongRunning);
                else
                    tasks[i] = Task.Factory.StartNew(actionWith, TaskCreationOptions.LongRunning);
            }

            tasks[9] = Task.Factory.StartNew(actionGet, TaskCreationOptions.LongRunning);

            Console.ReadLine();
        }
    }
    
    class BankAccount
    {
        private readonly AsyncLock _mutex = new AsyncLock();
        private int balance;

        public async Task Deposit(int amount)
        {
            using (await _mutex.LockAsync())
            {
                await Task.Delay(100);
                balance += amount;
                Console.WriteLine($"Deposit {amount}, current balance {balance}");   
            }
        }

        public async Task Withdraw(int amount)
        {
            using (await _mutex.LockAsync())
            {
                await Task.Delay(100);
                balance -= amount;
                Console.WriteLine($"Withdraw {amount}, current balance {balance}");   
            }
        }

        public async Task<int> Get()
        {
            using (await _mutex.LockAsync())
            {
                await Task.Delay(100);
                return balance;
            }
        }
    }
}