using System;
using System.Linq;
using System.Reflection;
using Reflections_13_1_Lib;

namespace Reflections_13_1
{
    public static class Exercises
    {
        
        // Dla podanej instancji klasy MyEntity nadpisz właściwość Secret na wartość "NotSoMuch" za pomocą 
        // refleksji i zwróć zmodyfikowany obiekt
        public static MyEntity E1(MyEntity entity)
        {
            typeof(MyEntity).GetProperty(nameof(MyEntity.Secret))
                ?.SetValue(entity, "NotSoMuch");
            return entity;
        }

        // Pobierz wszystkie metody wraz które posiada obiekt MyEntity i zwróć je jako tablica.
        public static MethodInfo[] E2()
        {
            return typeof(MyEntity).GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        }

        // Pobierz wartość atrybuty My i zwróć go jako wynik funkcji
        public static string E3()
        {
            var attr = System.Attribute.GetCustomAttributes(typeof(MyEntity))
                .Where(x => x is MyAttribute)
                .Cast<MyAttribute>()
                .First();
            
            return attr.Value;
        }

        // Stwórz i zwróć obiekt MyEntity za pomocą refleksji
        public static MyEntity E4()
        {
            return (MyEntity)Activator.CreateInstance(typeof(MyEntity), true);
        }

        // Za pomocą refleksji pobierz wszystkie typy danych, które implementują interfejs IMarker
        public static Type[] E5()
        {
            var type = typeof(IMarker);
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => !x.IsInterface)
                .Where(x => type.IsAssignableFrom(x))
                .ToArray();
        }

        // Za pomocą refleksji pobierz konstruktor z jednym parametrem typu string z klasy MyEntity
        // i za jego pomocą stwórz obiekt MyEntity
        public static MyEntity E6()
        {
            var t = typeof(MyEntity);
            var paramType = new Type[] { typeof(string) };
            var paramValue = new object[] { "Test Test" };
            var con = t.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, paramType, null);
            
            return (MyEntity)con?.Invoke(paramValue);
        }
    }
}