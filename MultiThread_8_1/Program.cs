﻿using System;
using System.Threading;

namespace MultiThread_8_1
{
    class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
            {
                Console.WriteLine("Not handled error inside thread");
            };
            
            // Za pomocą api Thread, stwórz dwa wątki background
            // Pierwszy z nich niech odlicza co sekundę w dół
            // Natomiast durgi z nich niech odlicza co sekundę w górę
            // W momencie kiedy pierwszy wątek osiągnie liczbę -10 zakończ wątek
            // W momencie kiedy drugi wątek osiągnie liczbę 15 zakończ wątek

            var t1 = new Thread(() =>
            {
                int i = 0;
                while (i > -10)
                {
                    DumpThreadInfo();
                    i--;
                    Console.WriteLine($"t1: {i}");
                    Thread.Sleep(1000);
                }
            }) { IsBackground = true };

            var t2 = new Thread(() =>
            {
                int i = 0;
                while (i < 15)
                {
                    DumpThreadInfo();
                    i++;
                    Console.WriteLine($"t2: {i}");
                    Thread.Sleep(1000);
                }
            }) { IsBackground = true };
            
            t1.Start();
            t2.Start();
            
            Console.ReadLine();
        }

        static void DumpThreadInfo()
        {
            Console.WriteLine($"Thread: id: {Thread.CurrentThread.ManagedThreadId} " +
                              $"isThreadPoolThread: {Thread.CurrentThread.IsThreadPoolThread} " +
                              $"isBackground: {Thread.CurrentThread.IsBackground}");
        }
    }
}