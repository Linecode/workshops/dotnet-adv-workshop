using System;

namespace IoC_Microsoft_WebApi.Services
{
    public interface IService {}
    public class Service1 : IService
    {
        
    }

    public interface IService2
    { }
    public class Service2 : IService2, IService {}
    
    public class Service3 : IService {}
    
    

    public class SomeType
    {
        public void SomeMethod()
        {
            Console.WriteLine("Some Log");
        }
    }
}