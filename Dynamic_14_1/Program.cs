﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;

namespace Dynamic_14_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var collection = new List<Person>
            {
                new Person { Id = Guid.NewGuid(), FirstName = "John", LastName = "Doe" },
                new Person { Id = Guid.NewGuid(), FirstName = "Jan", LastName = "Kowalski" }
            };
            
            var result = collection.ShapeData("Id,LastName");
            
            ((IDictionary<string, object>)result)
                .Add("Links", "https://example.com");

            Console.WriteLine("");
        }
    }
    
    // Napisz metodę rozszerzającą, która rozszerzy interfejs IEnumerable<T> o nazwie ShapeData
    // Metoda ma być metodą generyczną
    // Metoda ma zwracać IEnumerable<ExpandoObject>
    // Metoda ma przyjmować parametr string fields
    // W metodzie zaimplementuj mechanizm konwersji tej kolekcji na typ ExpandoObject, w którym
    // znajdą się propertisy zdefiniowane w zmiennej fields
    // Wywołanie tej metody ma wyglądać następująco: 
    // someCollection.ShapeData("someProps, otherProps")


    public static class EnumerableExtensions
    {
        public static IEnumerable<ExpandoObject> ShapeData<TSource>(this IEnumerable<TSource> source, string fields)
        {
            if (source is null) throw new Exception();
            var expandoObjectList = new List<ExpandoObject>();
            var propertyInfoList = new List<PropertyInfo>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                var propertyInfos = typeof(TSource)
                    .GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase);
                propertyInfoList.AddRange(propertyInfos);
            }
            else
            {
                var fieldsAfterSplit = fields.Split(",");

                foreach (var field in fieldsAfterSplit)
                {
                    var propertyName = field.Trim();

                    var propertyInfo = typeof(TSource)
                        .GetProperty(propertyName,
                            BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase);
                    if (propertyInfo is null) throw new Exception();
                    propertyInfoList.Add(propertyInfo);
                }
            }

            foreach (var obj in source)
            {
                var dataShapedObject = new ExpandoObject();

                foreach (var propertyInfo in propertyInfoList)
                {
                    var propertyValue = propertyInfo.GetValue(obj);
                    ((IDictionary<string, object>)dataShapedObject)
                        .Add(propertyInfo.Name, propertyValue);
                }
                
                expandoObjectList.Add(dataShapedObject);
            }

            return expandoObjectList;
        }
    }




    public class Person
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}