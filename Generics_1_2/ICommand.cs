namespace Generics_1_2
{
    public interface ICommand
    {
        
    }

    public interface ICommandHandler<T> where T : ICommand
    {
        void Handle(T command);
    }
}