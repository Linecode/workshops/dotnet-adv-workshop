# Generics 1_2

Twoim zadaniem będzie stworzenie bardzo prostego mechanizmu typu: command-handler 
tak aby kod zawarty w pliku `Program.cs` działał poprawnie.

- Zacznij od zapoznania się z kodem dostępnym w pliku `Program.cs` i katalogach `Domain` oraz `Infrastructure`
- Następnie zdefiniuj interfejsy dla obsługi komend
  - Stwórz interfejs, który będzie wskazywał że dana klasa / struktura jest komendą. Nazwij ten interfejs `ICommand`
  - Następnie stwórz interfejs do obsługi komend, nazwij go `ICommandHandler`. Pamiętaj aby zadeklarować że interfejs ICommandHandler przyjmuje typ generyczny T, który jest typem implementującym interfejs ICommand
  - Wewnątrz interfejsu ICommandHandler dodaj deklarację metody `Handle`, która jako swój parametr będzie przyjmować typ generyczny. Natomiast typem zwracanym w tym przypadku może być typ void.
- Następnie przejdź do utworzenia wymaganych interfejsów dla zapytań
  - Stwórz interfejs, który będzie wskazywał że dany typ jest zapytaniem. Nazwij go `IQuery`. W tym typie musisz jeszce dodać deklarację wykorzystania typu generycznego, aby tam zawrzec jaki typ danych będzie zwracany z zapytania
  - Następnie stwórz interfejs do obsługi zapytań. Nazwij go `IQueryHandler`. Pamiętaj aby zadeklarować że interfejs IQueryHandler przyjmuje dwa typu generyczne, pierwszy typ będzie typem zapytania, natomiast drugi będzie typem zwracanym przez metodę `Handle`. 
  - Dla deklaracji typów generycznych w interfejsie IQueryHandler dodaj wymaganie że pierwszy typ generyczny musi implementować interfejs IQuery<DrugiTypGeneryczny>
  - Wewnątrz interfejsu IQueryHandler dodaj deklarajcę metody `Handle`, która jako swój parametr będzie przyjmować pierwszy typ generyczny. Natomiast typem zwracanym będzie drugo typ generyczny.
- Stwórz katalogi `Commands`, `Handlers`, `Queries`
- W katalogu `Commands` zaimplementuj typ `CreateMovieCommand`. Pamiętaj aby zaimplementować wszystkie potrzebne do ustawienia propertisy oraz zaimplementować interfejs `ICommand`
- W katalogu `Queries` zaimplementuj typ `GetMovieQuery`. Pamiętaj aby zaimplementować w nim propertis `MovieId` oraz interfejs IQuery<T>
- W katalogu `Handlers` stwórz dwie klasy `CreateMovieHandler` oraz `GetMovieHandler`
  - W obu klasach przyjmij instancję klasy `Repository` przez konstruktor
  - W klasie `CreateMovieHandler` zaimplementuj odpowiedni interfejs `ICommandHandler`
  - W klasie `GetMovieHandler` zaimplementuj odpowiedni interfejs `IQueryHandler`
  - W obu klasach zaimplementuj wnętrze metody `Handle` zgodnie z przeznaczeniem. Czyli `CreateMovie` ma tworzyć nową instancję klasy Movie i dodawać ją do repozytorium. Natomiast `GetMovie` ma pobierać instancję klasy `Movie` z repozytorium.
- Uruchom program aby zweryfikować czy wszystko działa poprawnie


## Generics 1_3* (dla chętnych)

Spróbuj przepisać obecną implemetnację komend i handler-ów na wykorzystanie klas Result i Result<T> z poprzedniego ćwiczenia.