using System;
using Generics_1_2.Domain;

namespace Generics_1_2.Queries
{
    public class GetMovieQuery : IQuery<Movie>
    {
        public Guid Id { get; }

        public GetMovieQuery(Guid id)
        {
            Id = id;
        }
    }
}