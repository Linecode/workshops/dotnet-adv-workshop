using Generics_1_2.Domain;
using Generics_1_2.Infrastructure;
using Generics_1_2.Queries;

namespace Generics_1_2.Handlers
{
    public class GetMovieHandler : IQueryHandler<GetMovieQuery, Movie>
    {
        private readonly Repository _repository;

        public GetMovieHandler(Repository repository)
        {
            _repository = repository;
        }
        
        public Movie Handle(GetMovieQuery query)
        {
            return _repository.Get(query.Id);
        }
    }
}