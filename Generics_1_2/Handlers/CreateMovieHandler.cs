using Generics_1_2.Commands;
using Generics_1_2.Domain;
using Generics_1_2.Infrastructure;

namespace Generics_1_2.Handlers
{
    public class CreateMovieHandler : ICommandHandler<CreateMovieCommand>
    {
        private readonly Repository _repository;

        public CreateMovieHandler(Repository repository)
        {
            _repository = repository;
        }
        
        public void Handle(CreateMovieCommand command)
        {
            var movie = new Movie
            {
                Id = command.Id,
                Cast = command.Cast,
                Director = command.Director,
                Title = command.Title
            };
            
            _repository.Add(movie);
        }
    }
}