using System;
using System.Collections.Generic;
using Generics_1_2.Domain;

namespace Generics_1_2.Commands
{
    public class CreateMovieCommand : ICommand
    {
        public Guid Id { get; } 
        public string Title { get; }
        public MovieParty Director { get; }
        public List<MovieParty> Cast { get; }

        public CreateMovieCommand(Guid id, string title, MovieParty director, List<MovieParty> cast)
        {
            Id = id;
            Title = title;
            Director = director;
            Cast = cast;
        }
    }
}