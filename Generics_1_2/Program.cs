﻿using System;
using System.Linq;
using Generics_1_2.Commands;
using Generics_1_2.Domain;
using Generics_1_2.Handlers;
using Generics_1_2.Infrastructure;
using Generics_1_2.Queries;

namespace Generics_1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var repository = new Repository();
            
            #region DisplayRepository 
            Console.WriteLine("Generated Repository");
            foreach (var mov in repository.GetAll())
                Console.WriteLine(mov);
            #endregion
            

            var movieId = Guid.NewGuid();
            var createMovieCommand = new CreateMovieCommand(movieId ,"Lorem", new MovieParty
            {
                Id = Guid.NewGuid(),
                Role = MoviePartyRole.Director,
                FirstName = "Lorem",
                LastName = "Lipsum",
            }, null);
            var createMovieHandler = new CreateMovieHandler(repository);
            createMovieHandler.Handle(createMovieCommand);

            var getMovieQuery = new GetMovieQuery(movieId);
            var queryHandler = new GetMovieHandler(repository);

            var movie = queryHandler.Handle(getMovieQuery);
            
            Console.WriteLine(movie);
        }
    }
}