﻿using System;

namespace Delegates_Playground
{
    public delegate int MathFunction(int x);
    
    class Program
    {
        static void Main(string[] args)
        {
            var res = Calculator.Calculate((x, y) => x + y, 1, 2, 3);
            //var res = Calculator.Calculate(delegate(int x, int y) { return x + y; }, 1, 2, 3);
            Console.WriteLine(res);
        }
    }

    public static class Calculator
    {
        public static int Calculate(Func<int, int, int> function, params int[] source)
        {
            int result = 0;

            for (var i = 0; i < source.Length - 1; i++)
            {
                result += function.Invoke(source[i], source[i + 1]);
            }

            return result;
        }
    }

    // public class Func<T, params TX[>
    // {
    //     
    // }
}